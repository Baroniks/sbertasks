import sqlite3
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class Contact(BaseModel):
    id: int
    name: str

@app.get("/contacts/{search_query}")
def get_contacts(search_query: str):
    conn = sqlite3.connect("contacts.db")
    cursor = conn.cursor()

    # Создание таблицы, если она не существует
    try:
        cursor.execute("CREATE TABLE IF NOT EXISTS contacts (id INTEGER PRIMARY KEY, name TEXT)")
    except sqlite3.OperationalError:
        pass

    with open("contacts.txt", "r") as file:
        for line in file:
            id, name = line.split(";")
            cursor.execute("INSERT INTO contacts VALUES (?, ?)", (int(id), name))

    conn.commit()
    conn.close()

    result = []
    query = search_query.lower()

    for contact in cursor.execute("SELECT * FROM contacts WHERE name LIKE ?", ("%" + query + "%",)):
        result.append({"ID": contact[0], "NAME": contact[1]})

    if len(result) == 0:
        return {"result": []}
    else:
        return {"result": result}

@app.post("/log")
def log_request(request: dict):
    conn = sqlite3.connect("logs.db")
    cursor = conn.cursor()

    cursor.execute("INSERT INTO logs (request) VALUES (?)", (request,))
    conn.commit()
    conn.close()

    return {"success": True}